const input1 = formatMessage => ({
    name: formatMessage({
        id: 'INPUT.name',
        default: 'INPUT'
    }),
    extensionId: 'input1',
    version: '1.0.0',
    supportDevice: ['arduinoUno1', 'LGT8F328P1'],
    author: 'Liu',
    iconURL: `asset/INPUT1.jpg`,
    description: formatMessage({
        id: 'INPUT.description',
        default: 'INPUT drive module.'
    }),
    featured: true,
    blocks: 'blocks.js',
    generator: 'generator.js',
    toolbox: 'toolbox.js',
    msg: 'msg.js',
    library: 'lib',
    tags: ['actuator'],
    helpLink: ''
});

module.exports = input1;
