/* eslint-disable func-style */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
function addToolbox () {
    return `
<category name="%{BKY_INPUT_CATEGORY}" id="INPUT_CATEGORY" colour="#B943FF" secondaryColour="#9900FF">
    <block type="infrared1_value" id="infrared1_value">
        <field name="PIN">11</field>
    </block>
    <block type="sound1_value" id="sound1_value">
        <field name="portsAD">A0</field>
    </block>
    <block type="lightsensor1_value" id="lightsensor1_value">
        <field name="PORT">11</field>
    </block>
    <block type="button1_value" id="button1_value">
        <field name="PORT">11</field>
    </block>
    <block type="ultrasonic1_init" id="ultrasonic1_init">
        <field name="TPORT">14</field>
        <field name="EPORT">11</field>
    </block>
    <block type="ultrasonic1_value" id="ultrasonic1_value">
        <field name="TPORT">14</field>
    </block>
    <block type="color1_value2" id="color1_value2">
        <field name="COLOR">0</field>
    </block>
    <block type="color1_value" id="color1_value">
        <field name="COLOR">2</field>
    </block>
</category>`;
}

exports = addToolbox;
