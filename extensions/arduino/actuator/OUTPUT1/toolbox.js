/* eslint-disable func-style */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
function addToolbox () {
    return `
<category name="%{BKY_OUTPUT1_CATEGORY}" id="OUTPUT1_CATEGORY" colour="#FF6F00" secondaryColour="#FF4F00">
    <block type="output1_motor" id="output1_motor">
        <field name="Pos">1</field>
        <field name="Direction">1</field>
        <value name="SPEED">
            <shadow type="math_whole_number">
                <field name="NUM">0</field>
            </shadow>
        </value>
    </block>
    <block type="output1_led" id="output1_led">
        <field name="digiPinLED">4</field>
        <field name="onoff">1</field>
    </block>
    <block type="output1_led2" id="output1_led2">
        <field name="digiPinLED">4</field>
        <field name="onoff">1</field>
    </block>
    <block type="OUTPUT1_buzzer" id="OUTPUT1_buzzer">
        <field name="ports">11</field>
        <field name="onoff">1</field>
    </block>
    <block type="OUTPUT1_buzzer2" id="OUTPUT1_buzzer2">
        <field name="ports">11</field>
        <field name="hz">65</field>
        <field name="times">0</field>
    </block>
    <block type="OUTPUT1_servo" id="OUTPUT1_servo">
        <field name="ports">11</field>
        <value name="VALUE">
            <shadow type="math_whole_number">
                <field name="NUM">0</field>
            </shadow>
        </value>
    </block>
</category>`;
}

exports = addToolbox;
