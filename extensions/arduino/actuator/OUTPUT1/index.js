const output1 = formatMessage => ({
    name: formatMessage({
        id: 'OUTPUT.name',
        default: 'OUTPUT'
    }),
    extensionId: 'output1',
    version: '1.0.0',
    supportDevice: ['arduinoUno1', 'LGT8F328P1'],
    author: 'Liu',
    iconURL: `asset/OUTPUT1.jpg`,
    description: formatMessage({
        id: 'OUTPUT.description',
        default: 'OUTPUT drive module.'
    }),
    featured: true,
    blocks: 'blocks.js',
    generator: 'generator.js',
    toolbox: 'toolbox.js',
    msg: 'msg.js',
    library: 'lib',
    tags: ['actuator'],
    helpLink: ''
});

module.exports = output1;
